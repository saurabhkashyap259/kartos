package com.mate.kratos.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mate.kratos.Models.NavDrawerItem;
import com.mate.kratos.R;
import com.mate.kratos.ViewHolder.NavDrawerItemViewHolder;

import java.util.List;

/**
 * Created by sasuke on 14/6/16.
 */
public class NavDrawerListAdapter extends BaseAdapter {

    Activity activity;
    List<NavDrawerItem> navDrawerItemList;
    LayoutInflater layoutInflater;

    public NavDrawerListAdapter(Activity activity, List<NavDrawerItem> navDrawerItemList) {

        this.activity = activity;
        this.navDrawerItemList = navDrawerItemList;
    }

    @Override
    public int getCount() {
        return navDrawerItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        NavDrawerItemViewHolder holder = new NavDrawerItemViewHolder();

        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (row == null) {
            row = layoutInflater.inflate(R.layout.list_view_items_drawer, parent, false);
            // initialize the elements
            holder.itemID = (TextView) row.findViewById(R.id.item_id);
            holder.emailTitle = (TextView) row.findViewById(R.id.email_title);
            holder.image = (ImageView) row.findViewById(R.id.image);


            row.setTag(holder);
        } else {
            holder = (NavDrawerItemViewHolder) row.getTag();
        }

        // getting customer data for the row
        NavDrawerItem n = navDrawerItemList.get(position);

        if (n != null) {
            // customer name
            holder.itemID.setText(String.valueOf(n.getItemID()));
            holder.emailTitle.setText(n.getEmailTitle());

            if (n.isImageVisible()) {

                holder.image.setVisibility(View.VISIBLE);
            }else {

                holder.image.setVisibility(View.GONE);
            }

            if(n.getItemID() == 2) {

                holder.image.setBackground(activity.getResources().getDrawable(R.drawable.circular_background_2));
            }else if(n.getItemID() == 3) {

                holder.image.setBackground(activity.getResources().getDrawable(R.drawable.circular_background_3));
            }
        }

        return row;
    }
}
