package com.mate.kratos.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mate.kratos.Models.Message;
import com.mate.kratos.Models.NavDrawerItem;
import com.mate.kratos.R;
import com.mate.kratos.ViewHolder.MessageViewHolder;
import com.mate.kratos.ViewHolder.NavDrawerItemViewHolder;

import java.util.List;

/**
 * Created by sasuke on 14/6/16.
 */
public class MessageListAdapter extends BaseAdapter {

    Activity activity;
    List<Message> messageList;
    LayoutInflater layoutInflater;

    public MessageListAdapter(Activity activity, List<Message> messageList) {

        this.activity = activity;
        this.messageList = messageList;
    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public Object getItem(int position) {
        return messageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MessageViewHolder holder = new MessageViewHolder();

        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (row == null) {
            row = layoutInflater.inflate(R.layout.list_view_messages, parent, false);
            // initialize the elements
            holder.messageID = (TextView) row.findViewById(R.id.message_id);
            holder.participantName = (TextView) row.findViewById(R.id.participant_names);
            holder.subject = (TextView) row.findViewById(R.id.subject);
            holder.preview = (TextView) row.findViewById(R.id.preview);
            holder.starImage = (ImageView) row.findViewById(R.id.star_image);
            holder.messageLayout = (RelativeLayout) row.findViewById(R.id.message_layout);

            row.setTag(holder);
        } else {
            holder = (MessageViewHolder) row.getTag();
        }

        // getting customer data for the row
        Message m = messageList.get(position);

        if (m != null) {
            // customer name
            holder.messageID.setText(String.valueOf(m.getMessageID()));
            holder.participantName.setText(m.getParticipant());
            holder.subject.setText(m.getSubject());
            holder.preview.setText(m.getPreview());

            if (m.isStarred()) {

                holder.starImage.setImageResource(R.drawable.ic_action_action_grade_red);
            }

            if(!m.isRead()) {

                holder.messageLayout.setBackgroundColor(activity.getResources().getColor(R.color.color_white));
            }
        }

        return row;
    }
}
