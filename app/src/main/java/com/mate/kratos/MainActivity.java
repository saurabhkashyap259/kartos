package com.mate.kratos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mate.kratos.Adapter.NavDrawerListAdapter;
import com.mate.kratos.Fragments.AllInboxesFragment;
import com.mate.kratos.Fragments.GMailFragment;
import com.mate.kratos.Fragments.OutlookMailFragment;
import com.mate.kratos.Fragments.YahooMailFragment;
import com.mate.kratos.Models.NavDrawerItem;
import com.mate.kratos.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static FragmentManager fragmentManager;
    private static FragmentTransaction fragmentTransaction;
    private static Fragment previousFragment;
    private static ArrayList<Fragment> fragmentsOnStack = new ArrayList<>();
    private static int backCount;
    private static CharSequence drawerTitle;
    private static CharSequence title;
    private static ActionBarDrawerToggle drawerToggle;
    private static DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Set attribute for app name in toolbar
        assert toolbar != null;
        toolbar.setTitleTextColor(getResources().getColor(R.color.color_white));

        //Local variables
        ListView sliderMenu;
        List<NavDrawerItem> navDrawerItemList;
        NavDrawerListAdapter adapter;

        //get the ids
        sliderMenu = (ListView) findViewById(R.id.list_slider_menu);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        //get the items
        navDrawerItemList = getNavDrawerItems();

        //adapter
        adapter = new NavDrawerListAdapter(this, navDrawerItemList);

        //set adapter
        assert sliderMenu != null;
        sliderMenu.setAdapter(adapter);

        sliderMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                openFragment(position);
                drawerLayout.closeDrawers();
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open, R.string.drawer_close){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        //Fragment Manager
        fragmentManager = getSupportFragmentManager();

        openFragment(Constants.YahooMailFargment);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static void openFragment(int fragmentCount) {

        switch (fragmentCount) {

            case Constants.AllInboxesFragment:
                openFragment(new AllInboxesFragment());
                break;

            case Constants.YahooMailFargment:
                openFragment(new YahooMailFragment());
                break;

            case Constants.GMailFragment:
                openFragment(new GMailFragment());
                break;

            case Constants.OutlookMailFragment:
                openFragment(new OutlookMailFragment());
                break;

            default:
                break;
        }
    }

    public static void openFragment(Fragment frag) {

        //back pressed
        backCount = 1;

        // frame.removeAllViews();
        if (previousFragment != null) {

            destroyFragment(previousFragment);

        }

        if (fragmentsOnStack.size() > 0) {
            for (int i = 0; i < fragmentsOnStack.size(); i++) {
                destroyFragment(fragmentsOnStack.get(i));
            }

            fragmentsOnStack = new ArrayList<>();
        }

        fragmentTransaction = fragmentManager.beginTransaction();
        // /ft.setCustomAnimations(R.anim.slide_in_right,
        // R.anim.slide_out_left);
        fragmentTransaction.replace(R.id.container_body, frag);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        fragmentTransaction.commit();
        previousFragment = frag;

    }

    public static void destroyFragment(Fragment frag) {
        // frame.removeAllViews();

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(frag);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

        fragmentTransaction.commit();
    }

    public static List<NavDrawerItem> getNavDrawerItems() {

        List<NavDrawerItem> navDrawerItemList = new ArrayList<>();

        navDrawerItemList.add(new NavDrawerItem(0, "All Inboxes", false));
        navDrawerItemList.add(new NavDrawerItem(1, "Yahoo", true));
        navDrawerItemList.add(new NavDrawerItem(2, "GMail", true));
        navDrawerItemList.add(new NavDrawerItem(3, "Outlook", true));

        return navDrawerItemList;
    }
}
