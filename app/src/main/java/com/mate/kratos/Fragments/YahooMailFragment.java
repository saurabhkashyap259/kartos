package com.mate.kratos.Fragments;

import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mate.kratos.Adapter.MessageListAdapter;
import com.mate.kratos.AppController.AppController;
import com.mate.kratos.MainActivity;
import com.mate.kratos.Models.Message;
import com.mate.kratos.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sasuke on 14/6/16.
 */
public class YahooMailFragment extends Fragment {

    private static final String TAG = YahooMailFragment.class.getSimpleName();

    private static List<Message> messageList = new ArrayList<>();
    private static MessageListAdapter adapter;

    public YahooMailFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_yahoo_mail, container, false);

        getActivity().setTitle("Yahoo");
        ((MainActivity) this.getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_cyan_d500)));

        //status bar background color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.color_cyan_d800));
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState){
        super.onViewCreated(rootView, savedInstanceState);

        //Local Variables
        ListView messageListView;

        //Get the ids
        messageListView = (ListView) rootView.findViewById(R.id.list_view_messages);

        //adapter
        adapter = new MessageListAdapter(getActivity(), messageList);

        //set the adapter
        messageListView.setAdapter(adapter);

        if(!messageList.isEmpty()) {

            messageList.clear();
        }

        class SendJsonDataRequest extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(final Void... params) {

                JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("https://api.myjson.com/bins/1ltms", new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        Log.e(TAG, "Response: " + response);

                        for(int i=0; i<response.length(); i++) {

                            List<String> names = new ArrayList<>();
                            Message message = new Message();

                            try {
                                JSONObject jsonObject = response.getJSONObject(i);

                                JSONArray participants = jsonObject.getJSONArray("participants");

                                for(int j=0; j<participants.length(); j++) {

                                    String name = participants.getString(j);
                                    names.add(name);
                                }
                                String participantText = "";
                                for(String name : names) {

                                    participantText = participantText + name + ", ";
                                }

                                message.setParticipant(participantText.substring(0, participantText.length()-2));
                                message.setSubject(jsonObject.getString("subject"));
                                message.setPreview(jsonObject.getString("preview"));
                                message.setRead(jsonObject.getBoolean("isRead"));
                                message.setStarred(jsonObject.getBoolean("isStarred"));

                                messageList.add(message);

                                adapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyLog.e(TAG, "Error: " + error.getMessage());
                        Log.e("Json Array Error","Not Working");
                    }
                });

                AppController.getInstance().addToRequestQueue(jsonArrayRequest);
                return null;
            }
        }

        new SendJsonDataRequest().execute();
    }
}
