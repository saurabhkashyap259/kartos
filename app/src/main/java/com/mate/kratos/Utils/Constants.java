package com.mate.kratos.Utils;

/**
 * Created by sasuke on 14/6/16.
 */
public class Constants {

    public static final int AllInboxesFragment = 0;
    public static final int YahooMailFargment = 1;
    public static final int GMailFragment = 2;
    public static final int OutlookMailFragment = 3;

    public static final String BASE_URL = "http://127.0.0.1:<8088>";
    public static final String MESSAGE_LIST_URL = BASE_URL + "/api/message/";
}
