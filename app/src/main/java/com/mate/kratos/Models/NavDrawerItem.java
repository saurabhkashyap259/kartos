package com.mate.kratos.Models;

/**
 * Created by sasuke on 14/6/16.
 */
public class NavDrawerItem {

    int itemID;
    String emailTitle;
    boolean isImageVisible;

    public NavDrawerItem() {
    }

    public NavDrawerItem(int itemID, String emailTitle, boolean isImageVisible) {
        this.itemID = itemID;
        this.emailTitle = emailTitle;
        this.isImageVisible = isImageVisible;
    }

    public int getItemID() {
        return itemID;
    }

    public String getEmailTitle() {
        return emailTitle;
    }

    public boolean isImageVisible() {
        return isImageVisible;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public void setEmailTitle(String emailTitle) {
        this.emailTitle = emailTitle;
    }

    public void setImageVisible(boolean imageVisible) {
        isImageVisible = imageVisible;
    }
}
