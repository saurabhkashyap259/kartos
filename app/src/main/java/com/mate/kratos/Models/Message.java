package com.mate.kratos.Models;

/**
 * Created by sasuke on 14/6/16.
 */
public class Message {

    int messageID;
    String subject;
    String participant;
    String preview;
    boolean isRead;
    boolean isStarred;

    public Message() {
    }

    public Message(int messageID, String subject, String participant, String preview, boolean isRead, boolean isStarred) {
        this.messageID = messageID;
        this.subject = subject;
        this.participant = participant;
        this.preview = preview;
        this.isRead = isRead;
        this.isStarred = isStarred;
    }

    public int getMessageID() {
        return messageID;
    }

    public String getSubject() {
        return subject;
    }

    public String getParticipant() {
        return participant;
    }

    public String getPreview() {
        return preview;
    }

    public boolean isRead() {
        return isRead;
    }

    public boolean isStarred() {
        return isStarred;
    }

    public void setMessageID(int messageID) {
        this.messageID = messageID;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setParticipant(String participant) {
        this.participant = participant;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public void setStarred(boolean starred) {
        isStarred = starred;
    }
}
