package com.mate.kratos.ViewHolder;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by sasuke on 14/6/16.
 */
public class NavDrawerItemViewHolder {

    public TextView emailTitle;
    public TextView itemID;
    public ImageView image;
}
