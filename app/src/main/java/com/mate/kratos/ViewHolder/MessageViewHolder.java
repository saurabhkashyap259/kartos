package com.mate.kratos.ViewHolder;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by sasuke on 14/6/16.
 */
public class MessageViewHolder {

    public TextView messageID;
    public TextView participantName;
    public TextView subject;
    public TextView preview;
    public ImageView starImage;
    public RelativeLayout messageLayout;
}
